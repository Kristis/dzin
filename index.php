
<!DOCTYPE html>
<html lang="en">
  <head>

	 <link rel="stylesheet" href="style.css">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mūsų pirmas</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">	

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php  
  $bg = array('photo2.jpg', 'photo6.jpg', 'photo5.jpg' ); // array of filenames  

  $i = rand(0, count($bg)-1); // generate random number size of the array  
  $selectedBg = "$bg[$i]"; // set variable equal to which random filename was chosen  
?>

  </head>
<body>

<?php $connection = new mysqli ('localhost','root','', 'project');
if ($connection->connect_error){
	echo 'prisijungti nepavyko';
	} else {
	$sql="SELECT id, title01, text FROM dzin";
	$sql1="SELECT icon, text, text2, button, data FROM apacia ORDER BY RAND()";
	$sql2="SELECT icon, text, text2, button, data FROM apacia ORDER BY RAND()";
	$sql3="SELECT icon, text, text2, button, data FROM apacia ORDER BY RAND()";
$result = $connection->query($sql);
$result1 = $connection->query($sql1);
$result2 = $connection->query($sql2);
$result3 = $connection->query($sql3);
if ($result->num_rows > 0) {
	//print_r($result->fetch_assoc());

    //while($row = $result->fetch_assoc()) {
    //    echo "id: " . $row["id"]. " - Name: " . $row["title01"]. " " . $row["text"]. "<br>";
    //}
	$result = $result->fetch_assoc();
}
if ($result1->num_rows > 0) {	
$result1 = $result1->fetch_assoc();
	
} 
if ($result2->num_rows > 0) {	
$result2 = $result2->fetch_assoc();
	
}
if ($result3->num_rows > 0) {	
$result3 = $result3->fetch_assoc();
	
}else {
    echo "0 results<br>";
}
	}
	?>

	
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      
      <a class="navbar-brand" href=""><img src="img/musulogo.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
      
      <ul class="nav navbar-nav navbar-right">
        <li><a href="">Index</a></li>
		<li><a href="newsmain.html">News</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About us <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="person.html">Rokas</a></li>
            <li><a href="#">Giedrius</a></li>
            <li><a href="#">Juste</a></li>
            
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
		
		<ol class="breadcrumb">
  <li><a href="">Home</a></li>
		</ol>
<style>
.parallax {
   
    background-image: url("img/<?php echo $selectedBg; ?>");
    min-height: 300px; 
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
</style>
<div class="parallax">
<br><br>
<div class="container" style="opacity:0.5; background-color: white">
       
        <h1 class="col-md-12 black centered" id="title01"> <?php echo $result['title01']; ?></h1><br>
        
         <h4 id="text"> <?php echo $result['text'];?></h4>
        
        <div class="row col-md-offset-5">
          <button type="button" class="btn btn-warning btn-lg" style="color: black">Toliau</button>
        </div>
    </div>

	</div> 
	<br>
    <div class="row centered">
			<div class="col-md-3 col-md-offset-2">
			<span style="font-size:5em;" class="glyphicon glyphicon-<?php echo $result1['icon'] ?>"></span>
			<p><?php echo $result1['text'] ?></p>
			<p><strong><?php echo $result1['text2'] ?></strong></p>
			<p><i>Daugiau</i> <u>skaityti</u><a href="http://www.delfi.lt"> čia</a><p>
			<button type="button" class="btn btn-danger"><?php echo $result1['button'] ?></button>
			<p><h6><i>Irašo laikas <?php echo $result1['data'] ?></h6></i></div>
			
			<div class="col-md-3">
			<span style="font-size:5em;" class="glyphicon glyphicon-<?php echo $result2['icon'] ?>"></span>
			<p><?php echo $result2['text'] ?></p>
			<p><strong><?php echo $result2['text2'] ?></strong></p>
			<p><i>Daugiau</i> <u>skaityti</u><a href="http://www.lrytas.lt"> čia</a><p>
			<button type="button" class="btn btn-danger"><?php echo $result2['button'] ?></button>
			<p><h6><i>Irašo laikas <?php echo $result2['data'] ?></h6></i></div>
			
			<div class="col-md-3">
			<span style="font-size:5em;" class="glyphicon glyphicon-<?php echo $result3['icon'] ?>"></span>
			<p><?php echo $result3['text'] ?></p>
			<p><strong><?php echo $result3['text2'] ?></strong></p>
			<p><i>Daugiau</i> <u>skaityti</u><a href="http://www.15min.lt"> čia</a><p>
			<button type="button" class="btn btn-danger"><?php echo $result3['button'] ?></button>
			<p><h6><i>Irašo laikas <?php echo $result3['data'] ?></h6></i></div>
			</div>
			
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
	  <script type="text/javascript" src="new 1.js"></script>
	  
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>