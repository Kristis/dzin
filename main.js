$(document).ready(function(){
	$('.carousel').carousel();
	
/* tabs in about us */
	$("#giedrius").click(function() {
		$("#tab1").hide();
		$("#tab2").hide();
		$("#tab3").hide();
		$("#tab2").show();
		document.getElementById("giedrius").className = "active";
		document.getElementById("rokas").className = "";
		document.getElementById("juste").className = "";
	})
	$("#juste").click(function() {
		$("#tab1").hide();
		$("#tab2").hide();
		$("#tab3").hide();
		$("#tab3").show();
		document.getElementById("juste").className = "active";
		document.getElementById("rokas").className = "";
		document.getElementById("giedrius").className = "";
	})
	$("#rokas").click(function() {
		$("#tab1").hide();
		$("#tab2").hide();
		$("#tab3").hide();
		$("#tab1").show();
		document.getElementById("giedrius").className = "";
		document.getElementById("rokas").className = "active";
		document.getElementById("juste").className = "";
	})
/* google maps */
function initialize() {
  var mapProp = {
    center:new google.maps.LatLng(54.687157, 25.279652),
    zoom:12,
    mapTypeId:google.maps.MapTypeId.HYBRID
  };
  var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
google.maps.event.addDomListener(window, 'load', initialize);
}) 